package org.example.enity;

import org.example.database.DataBaseConfig;
import org.example.enity.Car;
import org.example.enity.Driver;
import org.example.enity.Truck;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {

        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Car c = new Car();
        c.setId(11);
        c.setName("Test car");

        Car c2 = new Car();
        c2.setId(33);
        c2.setName("Second test");

        session.persist(c); // un fel de INSERT
        session.persist(c2); // un fel de INSERT

        transaction.commit();
        session.close();
    }
}