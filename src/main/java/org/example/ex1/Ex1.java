package org.example.ex1;

import org.example.database.DataBaseConfig;
import org.example.enity.Car;
import org.example.enity.Driver;
import org.example.enity.Truck;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;

public class Ex1 {

    public static void main(String[] args) {
        // adăugați 2 filme și 3 actori într-o singură tranzacție
        // - creare session factory
        // - creare tabele
        // inserare date

        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Movie m = new Movie(1, "Titannic", Genre.ROMANCE, LocalDate.of(1990, 12, 4), 8.7, 100000000.0);
        session.persist(m);


        transaction.commit();
        session.close();
    }
}
