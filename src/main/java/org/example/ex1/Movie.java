package org.example.ex1;

/*
- id
- numele
- genul
- release date
- imdb score
- incasari
 */


import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;

import java.time.LocalDate;

@Entity
public class Movie {

    @Id
    private Integer id;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Genre genre;

    private LocalDate releaseDate;

    private Double score;

    private Double cashin;


    public Movie() {
    }

    public Movie(Integer id, String name, Genre genre, LocalDate releaseDate, Double score, Double cashin) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.score = score;
        this.cashin = cashin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getCashin() {
        return cashin;
    }

    public void setCashin(Double cashin) {
        this.cashin = cashin;
    }
}
