package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.relations.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class GeneratedIdMain {

    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Mother mom = new Mother(null, "Mami", Job.ENGINEER);
        Mother mom2 = new Mother(null, "Mami a doua", Job.LAWYER);
        System.out.println(mom.getId() + " " + mom2.getId());
        session.persist(mom);
        session.persist(mom2);
        System.out.println(mom.getId() + " " + mom2.getId());
        transaction.commit();

        session.close();
    }
}
