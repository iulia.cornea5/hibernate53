package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.relations.entity.Animal;
import org.example.relations.entity.Owner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;

public class ManyToOneMain {

    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Owner o1 = new Owner(null, "Mihai", new ArrayList<>());
        Animal cat = new Animal(null, "Suri", "cat", o1);
        o1.addPet(cat);
        session.persist(cat);


        transaction.commit();
        session.close();
    }
}
