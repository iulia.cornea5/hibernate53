package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.ex1.Genre;
import org.example.relations.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class OneToManyMain {

    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Hobby running = new Hobby(null, "Running", "Medium");
        Hobby painting = new Hobby(null, "Painting", "Hard");
        session.persist(running);
        session.persist(painting);

        TvShow bones = new TvShow(null, "Bones", Genre.ACTION);
        TvShow dosareleX = new TvShow(null, "Dosarele X", Genre.ACTION);
        // NU vrem să le persistăm deoarece operația de PERSIST este cascadată din Mother

        Mother mom = new Mother(null, "Mami", Job.ENGINEER, List.of(running, painting), List.of(bones, dosareleX));
        session.persist(mom);
        transaction.commit();

        session.close();
    }
}
