package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.relations.entity.Child;
import org.example.relations.entity.Food;
import org.example.relations.entity.Mother;
import org.example.relations.entity.Toy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class OneToOneMain {

    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();
        Food f1 = new Food(1, "milk", true);
        session.persist(f1);
//        session.persist(toy1); NU îl punem

        Toy toy1 = new Toy(1, "robot");
        Child c1 = new Child(1, "Johnny",  f1, toy1);
        session.persist(c1);
        t1.commit();
        System.out.println("Child was saved");

        Transaction t2 = session.beginTransaction();
        session.remove(c1);
        t2.commit();
        System.out.println("Child was removed");
        System.out.println("Check what happened with toy");
        session.close();
    }
}
