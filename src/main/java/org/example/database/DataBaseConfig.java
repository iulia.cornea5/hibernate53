package org.example.database;

import org.example.enity.Car;
import org.example.enity.Driver;
import org.example.enity.Truck;
import org.example.ex1.Actor;
import org.example.ex1.Movie;
import org.example.ex2.Student;
import org.example.relations.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DataBaseConfig {

    private static SessionFactory sessionFactory = null;

    private DataBaseConfig() {

    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Car.class)
                    .addAnnotatedClass(Driver.class)
                    .addAnnotatedClass(Truck.class)
                    .addAnnotatedClass(Actor.class)
                    .addAnnotatedClass(Movie.class)
                    .addAnnotatedClass(Student.class)
                    .addAnnotatedClass(Child.class)
                    .addAnnotatedClass(Food.class)
                    .addAnnotatedClass(Hobby.class)
                    .addAnnotatedClass(Mother.class)
                    .addAnnotatedClass(Toy.class)
                    .addAnnotatedClass(TvShow.class)
                    .addAnnotatedClass(Animal.class)
                    .addAnnotatedClass(Owner.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
