package org.example.ex2;

import org.example.database.DataBaseConfig;
import org.example.ex1.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Ex2 {

    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();

        Student s = new Student(1, "Ioana", 1987);
        session.persist(s);

        t1.commit();
        session.close();
    }
}
